<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@index');

Route::get('/article', 'ArticleController@create');
Route::post('/article', 'ArticleController@store');
Route::get('/articles/{id}', 'ArticleController@userArticles');
Route::get('/articles/{id}/like/toggle', 'ArticleController@toggleLike');

Route::get('/contact', 'ContactController@create');
Route::post('contact', 'ContactController@store');

Route::get('/comment', 'CommentController@create');
Route::post('/comment', 'CommentController@store');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
