<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Google News</title>
        <link rel="stylesheet" href="/css/app.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <?php if($message = session('content')): ?>
            <div class="alert alert-success">
                <?php echo $message ?>
            </div>
        <?php endif; ?>

        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error): ?>
                        <li><?php echo $error ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>


        @if(Auth::check())
            Hello, {{ Auth::user()->name }}
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>

        @else
            <a href ="/login">Login</a>
        @endif

        <header>


            <div class="top-nav">
                <div class="hamburger">
                    <button><i class="fa fa-bars fa-lg"></i></button>
                    <a href="/"><img src="/img/google-logo.jpg">News</a>
                </div>



                <div class="search-bar">
                    <span class ="icon"><i class="fa fa-search fa-lg"></i></span>
                    <input type="text" placeholder="Search"></input>
                </div>

                <div class ="side-nav">
                    <a href="/contact">Contact Us</a>
                    <i class="fa fa-th fa-lg"></i>
                    <i class="fa fa-bell fa-lg"></i>

                </div>
            </div>

            <div class ="bottom-nav">

                <div class="links">
                    <a href="#">Headlines </a>
                    <a href="#">Local </a>
                    <a href="#">For You </a>
                    <a href ="#">|</a>
                    <a href="#">Canada <i class="fa fa-angle-down fa-lg"></i></a>
                </div>
                <div class="icon">
                    <i class="fa fa-cog fa-2x"></i>
                </div>

            </div>

        </header>



        @yield('content')
    </body>
</html>
