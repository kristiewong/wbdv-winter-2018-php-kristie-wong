@extends('layout')

@section('content')
    <?php if($message = session('message')): ?>
        <div class="alert alert-success">
            <?php echo $message ?>
        </div>
    <?php endif; ?>

    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors->all() as $error): ?>
                    <li><?php echo $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <h1>Contact Form</h1>

    <form method="post">
        <?php echo csrf_field() ?>
        <input type="text" name="name" value="" placeholder="Name">
        <input type="text" name="email" value="" placeholder="Email"><br><br>
        <textarea name="message" rows="8" cols="80" placeholder="Enter Message Here..."></textarea><br>
        <input type="submit" name="" value="submit">
    </form>

@endsection
