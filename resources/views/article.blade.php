@extends('layout')

@section('content')

    <?php if($message = session('content')): ?>
        <div class="alert alert-success">
            <?php echo $message ?>
        </div>
    <?php endif; ?>

    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors->all() as $error): ?>
                    <li><?php echo $error ?> </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>


    <h1>Enter Your Article Here:</h1>

    <form method="post">
        <?php echo csrf_field() ?>
        <input type="text" name="title" value="" placeholder="Title"><br><br>
        <textarea name="content" rows="8" cols="80" placeholder="Enter Article Here..." class="form-control <?php echo $errors->has('content') ? 'is-invalid' : '' ?>"><?php echo old('content')?></textarea><br>
        <?php if($errors->has('content')): ?>
            <span class="invalid-feedback"><?php echo $errors->first('content')?></span>
        <?php endif; ?>
        <input type="submit" name="" value="submit">
    </form>

@endsection
