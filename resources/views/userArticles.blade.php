@extends('layout')

@section('content')

<div class="container">
    <div class="user-article">
        <?php foreach ($articles as $article): ?>
            <div class="articles">
                <div class ="image">
                    <img src="<?php echo $article->image ?>">
                </div>
                <div class ="content">
                    <h6><a href="#"><?php echo $article->title ?></a></h6>
                    <a href="/articles/{{$article->user->id}}"><span class="text"><?php echo $article->user->name ?></a> .
                        <?php echo $article->updated_at->diffForHumans() ?></span><br><br>
                        <?php echo $article->content ?></span><br><br>

                        @if ($article->isLikedByCurrentUser())
                            <a href="/articles/{{$article->id}}/like/toggle">Unlike</a>
                        @else
                            <a href="/articles/{{$article->id}}/like/toggle">Like</a>
                        @endif
                        <br>
                        Liked by <?php echo count ($article->likes) ?> users |
                        # of comments: <?php echo count ($article->comments) ?>
                        <br>
                        <hr>

                        <form class="" action="/comment" method="post">
                            <?php echo csrf_field() ?>
                            <textarea name="content" rows="4" cols="50" placeholder="Enter your comment here..."></textarea><br>
                            <input type="submit" name="" value="Comment" class="btn">
                            <input type="hidden" name="article_id" value="{{$article->id}}">
                        </form>

                        <hr>
                    </div>

                    <div class="comments">
                        <?php foreach ($article->comments as $comment): ?>
                            User: <a href="/articles/{{$article->id}}"><?php echo $comment->user->name ?></a><br>
                            Comment: <?php echo $comment->content ?><br>
                            <?php echo $comment->updated_at->diffForHumans() ?><br>
                            <hr>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php endforeach ?>
    </div>
</div>

@endsection
