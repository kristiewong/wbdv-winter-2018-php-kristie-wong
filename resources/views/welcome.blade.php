
@extends('layout')

@section('content')

    
    <div class="body container">

        <div class="sections">
            <p>SECTIONS</p>
            <ul>
                <li><a href="#"><i class="fa fa-calendar"></i> Top Stories</a></li>
                <li><a href="#"><i class="fa fa-globe"></i> World</a></li>
                <li><a href="#"><i class="fa fa-flag"></i> Canada</a></li>
                <li><a href="#"><i class="fa fa-building"></i> Business</a></li>
                <li><a href="#"><i class="fa fa-database"></i> Technology</a></li>
                <li><a href="#"><i class="fa fa-film"></i> Entertainment</a></li>
                <li><a href="#"><i class="fa fa-bicycle"></i> Sports</a></li>
                <li><a href="#"><i class="fa fa-flask"></i> Science</a></li>
                <li><a href="#"><i class="fa fa-heartbeat"></i> Health</a></li>
                <hr>
                <li><a href="#"><i class="fa fa-edit"></i> Manage sections</a></li>
                <hr>
            </ul>
            <a href="/article"><input type="button" name="" value="Enter Article" class="btn"></a>
        </div>

        <div class="stories">

            <h2>Top Stories</h2>

            <?php foreach ($articles as $article): ?>
                <div class="articles">
                    <div class ="image">
                        <img src="<?php echo $article->image ?>">
                    </div>
                    <div class ="content">
                        <h6><a href="#"><?php echo $article->title ?></a></h6>
                        <a href="/articles/{{$article->user->id}}"><span class="text"><?php echo $article->user->name ?></a> .
                            <?php echo $article->updated_at->diffForHumans() ?></span><br><br>
                            <?php echo $article->content ?></span><br><br>

                            @if ($article->isLikedByCurrentUser())
                                <a href="/articles/{{$article->id}}/like/toggle">Unlike</a>
                            @else
                                <a href="/articles/{{$article->id}}/like/toggle">Like</a>
                            @endif
                            <br>
                            Liked by <?php echo count ($article->likes) ?> users |
                            # of comments: <?php echo count ($article->comments) ?>
                            <br>
                            <hr>

                            <form class="" action="/comment" method="post">
                                <?php echo csrf_field() ?>
                                <textarea name="content" rows="4" cols="50" placeholder="Enter your comment here..."></textarea><br>
                                <input type="submit" name="" value="Comment" class="btn">
                                <input type="hidden" name="article_id" value="{{$article->id}}">
                            </form>

                            <hr>
                        </div>

                        <div class="comments">
                            <?php foreach ($article->comments as $comment): ?>
                                User: <a href="/articles/{{$article->id}}"><?php echo $comment->user->name ?></a><br>
                                Comment: <?php echo $comment->content ?><br>
                                <?php echo $comment->updated_at->diffForHumans() ?><br>
                                <hr>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endforeach ?>



            </div>


            <div class = "right-column">
                <h5>In The News</h5>
                <div class="in-the-news">
                    <?php foreach ($articles as $article): ?>
                        <button><?php echo $article->title ?></button><br>
                    <?php endforeach ?>

                </div>

                <h5>Recent</h5>
                <div class="recent">
                    <?php foreach ($articles as $article): ?>
                        <span class="text"><a href="#"><?php echo $article->title ?></a><br>

                            <?php echo $article->updated_at->diffForHumans() ?><br></span><br>
                        <?php endforeach ?>
                    </div>

                    <h5>Calgary</h5>
                    <div class="calgary">
                        <div class="weather">

                            <span="today"><strong>Today</strong></span>
                            <br>
                            <img id="icon"/>
                            <br/>
                            <span id="temp"></span>°


                            <script type="text/javascript">
                                // set up AJAX
                                var xhttp = new XMLHttpRequest();
                                // set up a URL variable to hold the URL to the API
                                var url = "http://api.openweathermap.org/data/2.5/weather?q=Calgary,ca&appid=c4703850194cf25ab30bc7dddd9d6af0";
                                // ajax event handler
                                xhttp.onreadystatechange = function(){
                                    // is the server done responding?
                                    if(xhttp.readyState==4 && xhttp.status==200){
                                        // set the parsed JSON into a variable
                                        var parsedJSON = JSON.parse(xhttp.responseText);
                                        //  call a function to output the weather data
                                        outputWeather(parsedJSON);
                                    }
                                }
                                xhttp.open("GET", url, true);
                                xhttp.send();

                                function outputWeather(weatherData){

                                    // output the image
                                    var icon = weatherData.weather[0].icon;
                                    icon = "https://openweathermap.org/img/w/"+icon+".png";
                                    document.getElementById('icon').src = icon;

                                    // get the temperature
                                    var temp = weatherData.main.temp;
                                    // change temp to an integer and subtract -273.15 to get celcius.
                                    temp = parseInt(temp-273.15);
                                    document.getElementById('temp').innerHTML = temp;
                                }
                            </script>
                        </div>

                        <?php foreach ($articles as $article): ?>
                            <span class="text"><a href="#"><?php echo $article->title ?></a><br>
                            <?php echo $article->user->name ?> .
                            <?php echo $article->updated_at->diffForHumans() ?><br></span><br>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>

    @endsection
