<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Article;
use App\User;

class CommentController extends Controller
{
    public function create(){
        if(!\Auth::check()){
            return redirect('/login');
        }
        return view('comment');
    }

    public function store(){

        if(!\Auth::check()){
            return redirect('/login');
        }

        $request = request();
        $result = $request->validate([
            'content' => 'required|max:400'],
            ['content.required' => 'Please enter your comment!'
        ]);

        $data = request()->all();
        $loggedInUser = $request->user();

        $comment = new Comment();
        $comment->user_id = $loggedInUser->id;
        $comment->article_id = $data['article_id'];
        $comment->content = $data['content'];
        $comment->save();

        return redirect('/');
    }

}
