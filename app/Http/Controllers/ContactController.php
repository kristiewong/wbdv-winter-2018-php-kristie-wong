<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create(){
        return view('contact');
    }

    public function store(){

        $request = request();

        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'message' => 'required|max:400'] ,
            ['name.required' => 'Please enter your name!',
            'email.required' => 'Please enter your email!',
            'message.required' => 'Please enter a message!']
        );


        $data = request()->all();

        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->email =$data['email'];
        $contact->message =$data['message'];
        $contact->save();

        return redirect('/contact')->with('message', 'Your message has successfully sent. Someone wil contact you shortly!');

    }
}
