<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;
use App\User;
use App\Models\Article;
use App\Models\Comment;


class IndexController extends Controller
{
    public function index(){
        $faker = Factory::create();

        $users = User::all();
        $articles = Article::all();
        $comments = Comment::all();

        $data = [
            'users' => $users,
            'articles' => $articles,
            'comments' => $comments
        ];

        return view('welcome', $data);
    }
}
